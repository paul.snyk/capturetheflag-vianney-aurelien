﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnPlayerScript : MonoBehaviour
{
    public GameObject playerBluePrefab;
    public GameObject playerRedPrefab;
    
    public GameObject[] playerCameraDeath = new GameObject[2];
    public GameObject[] playerSpawnPoint = new GameObject[2];
    
    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RespawnBluePlayer()
    {
        playerCameraDeath[1].SetActive(false);
        Instantiate(playerBluePrefab, playerSpawnPoint[1].transform);
    }
    public void RespawnRedPlayer()
    {
        playerCameraDeath[0].SetActive(false);
        Instantiate(playerRedPrefab, playerSpawnPoint[0].transform);
    }
}
