﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreController : MonoBehaviour
{
    //panel
    public GameObject[] defeatPanel = new GameObject[2];
    public GameObject[] victoryPanel = new GameObject[2];
    public TMP_Text[] textNextStep = new TMP_Text[2];
    public TMP_Text[] PlayerScoreText = new TMP_Text[2];
    

    //value
    public bool victoryCheck;
    public int redPlayerScore;
    public int bluePlayerScore;

    public flagSpawn _flagSpawn;
    
    // Start is called before the first frame update
    void Start()
    {
        // check value = 0/false
        victoryCheck = false;
        redPlayerScore = 0;
        bluePlayerScore = 0;
    }

    // Update is called once per frame
    void Update()
    {
        string redPlayer = redPlayerScore.ToString();
        PlayerScoreText[0].text = redPlayer;

        string bluePlayer = bluePlayerScore.ToString();
        PlayerScoreText[1].text = bluePlayer;
    
        //victory check
        if (bluePlayerScore == 5)
        {
            Time.timeScale = 0f;
            defeatPanel[0].SetActive(true);
            victoryPanel[1].SetActive(true);
            victoryCheck = true;
            textNextStep[0].text = "Appuyer sur Espace pour revenir au menu";
            textNextStep[1].text = "Appuyer sur Espace pour revenir au menu";
        }

        if (redPlayerScore == 5)
        {
            Time.timeScale = 0f;
            defeatPanel[1].SetActive(true);
            victoryPanel[0].SetActive(true);
            victoryCheck = true;
            textNextStep[1].text = "Appuyer sur Espace pour revenir au menu";
            textNextStep[0].text = "Appuyer sur Espace pour revenir au menu";
        }

        //input after victory
        if (victoryCheck)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                Debug.Log("retour menu principal après victoire");
            }
        }
    }
    
    //add score void
    public void BluePlayerScoreUpgrade(int currentBlueScore)
    {
        bluePlayerScore += currentBlueScore;
        Debug.Log("add score blue player");
        _flagSpawn.SpawnRedFlag();
    }
    
    public void RedPlayerScoreUpgrade(int currentRedScore)
    {
        _flagSpawn.SpawnBlueFlag();
        redPlayerScore += currentRedScore;
        Debug.Log("add score red player");
    }
}
