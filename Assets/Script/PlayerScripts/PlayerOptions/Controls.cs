﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Controls
{
    public KeyCode forwards, backwards, strafeLeft, strafeRight, rotateLeft, rotateRight, jump, fireShoot, itemRecuperation, item1, item2, walkOrRun;
}
