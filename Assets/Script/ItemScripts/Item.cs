﻿using UnityEngine;
/**
 * Item with name, icon, isDefault
 */
[CreateAssetMenu(fileName = "New item", menuName = "Inventory/Item")]
public class Item : ScriptableObject
{
    // Default values for a new item
    new public string name = "New item";
    public Sprite icon = null;
    public bool isDefault = false;
    public bool canHold = false;

    public void Use()
    {
        // Use the item
        // Something might happen
        Debug.Log("Using" + name);

        switch (name)
        {
            case "AnchorGrapple":
            {
                break;
            }
            case "Apple":
            {
                break;
            }
            case "Axe":
            {
                // allows three attacks with a 3 second cooldown
                break;
            }
            case "Flag":
            {
                break;
            }
            case "Mine":
            {
                
                break;
            }
            case "Revolver":
            {
                // Fire six shots
                break;
            }
            case "ShieldChestplate":
            {
                // blocks a shot (pistol or knife) and is destroyed when used
                break;
            }
            case "Shoes":
            {
                break;
            }
        }
    }
}
