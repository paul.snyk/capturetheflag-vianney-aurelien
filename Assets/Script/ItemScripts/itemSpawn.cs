﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawn : MonoBehaviour
{
    public Transform spawn1, spawn2;

    public GameObject[] item;

    public float timer;
    public int randomInt1, randomInt2;
    public GameObject item1, item2;
    
    
    // Start is called before the first frame update
    void Start()
    {
        timer = 30;
        randomChoice();
    }

    // Update is called once per frame
    void Update()
    {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }

        if (timer < 0)
        {
            //destruction des items si encore sur le terrain
            if (item1 != null)
            {
                Destroy(item1);
            }
            if (item2 != null)
            {
                Destroy(item2);
            }
            randomChoice();
            item1 = Instantiate(item[randomInt1], spawn1);
            item2 = Instantiate(item[randomInt2], spawn2);
            item1.transform.position = spawn1.position;
            item2.transform.position = spawn2.position;
            timer = 30;
        }
    }

    public void randomChoice()
    {
        randomInt1 = Random.Range(0, 6);
        randomInt2 = Random.Range(0, 6);
    }
}
