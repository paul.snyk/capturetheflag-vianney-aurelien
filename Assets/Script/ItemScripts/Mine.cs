﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : MonoBehaviour
{
    private bool isPosed = false;
    private bool isVisible = true;
    private GameObject player;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isPosed && isVisible)
        {
            HideMine();
        }

        Detection();
    }

    void HideMine()
    {
        // remove visibility
        GetComponent<Renderer>().enabled = false;
        isVisible = false;
    }

    void Detection()
    {
        RaycastHit hit;

        Vector3 position = transform.position;

        if (Physics.SphereCast(position, 2, transform.forward, out hit, 3))
        {
            Player1Controls player1 = hit.collider.GetComponent<Player1Controls>();
            Player2Controls player2 = hit.collider.GetComponent<Player2Controls>();
            // If the player1 is well found
            if (player1 != null)
            {
                Explode(1);
            }
            // If the player2 is well found
            else if (player2 != null)
            {
                Explode(2);
            }
        }
    }

    void Explode(int numPlayer)
    {
        if (numPlayer == 1)
        {
            player = GameObject.FindGameObjectWithTag("BluePlayer");
            player.GetComponent<HealthBluePlayer>().Damage(3);
        }
        else if (numPlayer == 2)
        {
            player = GameObject.FindGameObjectWithTag("RedPlayer");
            player.GetComponent<HealthRedPlayer>().Damage(3);
        }
        Destroy(this);
    }
}
