﻿using UnityEngine;

public class InventoryUI : MonoBehaviour
{
    private Inventory1 inventory1;
    private Inventory2 inventory2;
    private InventorySlot[] slots1;
    private InventorySlot[] slots2;

    public Transform itemsParent1;
    public Transform itemsParent2;
    
    // Start is called before the first frame update
    void Start()
    {
        // Instance
        inventory1 = Inventory1.instance;
        inventory1.onItemChangedCallback += UpdateUI;
        inventory2 = Inventory2.instance;
        inventory2.onItemChangedCallback += UpdateUI;
        
        // Get the InventorySlot
        slots1 = itemsParent1.GetComponentsInChildren<InventorySlot>();
        slots2 = itemsParent2.GetComponentsInChildren<InventorySlot>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void UpdateUI()
    {
        Debug.Log("Updating UI");
        for (int i = 0; i < slots1.Length; i++)
        {
            if (i < inventory1.items.Count)
            {
                // Displays the item in the UI 1
                slots1[i].AddItem(inventory1.items[i]);
            }
            else
            {
                // Clear the slots array 
                slots1[i].ClearSlot();
            }
        }
        
        for (int i = 0; i < slots2.Length; i++)
        {
            if (i < inventory2.items.Count)
            {
                // Displays the item in the UI 2
                slots2[i].AddItem(inventory2.items[i]);
            }
            else
            {
                // Clear the slots array 
                slots2[i].ClearSlot();
            }
        }
    }
}
