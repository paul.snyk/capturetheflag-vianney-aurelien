﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    // Radius around the item
    public float radius = 2f;

    public Transform interactionTransform;

    // Check if the item is focus
    private bool isFocus = false;

    // Transform of the player
    private Transform playerTransform;

    private bool hasInteraced = false;

    // Interact() is meant to be overwitten
    public virtual void Interact(string playerTag)
    {
        Debug.Log(playerTag + " interact with " + transform.name);
    }

    void Update()
    {
        // Get the distance between the player and the item position
        float distance = Vector3.Distance(playerTransform.position, interactionTransform.position);

        // If the player enter in the item radius
        if (distance <= radius)
        {
            Debug.Log("Good distance to take item");
            {
                Debug.Log("");
                Interact(playerTransform.gameObject.tag);
                hasInteraced = true;
            }
        }
    }
    
    // Set the playerTransform
    public void OnFocused(Transform playerTransform)
    {
        Debug.Log("OnFocused");
        isFocus = true;
        this.playerTransform = playerTransform;
        hasInteraced = false;
    }

    public void OnDefocused()
    {
        Debug.Log("OnDefocused");
        isFocus = false;
        playerTransform = null;
        hasInteraced = false;
    }

    // Selection of gizmos
    void OnDrawGizmosSelected()
    {
        if (interactionTransform == null)
        {
            interactionTransform = transform;
        }

        Gizmos.color = Color.yellow;
        // Sphere to detect something
        Gizmos.DrawWireSphere(interactionTransform.position, radius);
    }
}