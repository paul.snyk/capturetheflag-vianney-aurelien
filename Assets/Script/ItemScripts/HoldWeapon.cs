﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoldWeapon : MonoBehaviour
{
    public Transform handPlayer;
    public GameObject revolver;
    public GameObject axe;
    
    private GameObject currentWeapon;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void EquipWeapon(string name)
    {
        if (name.Equals("Revolver"))
        {
            currentWeapon = revolver;
        }
        else if(name.Equals("Axe"))
        {
            currentWeapon = axe;
        }

        if (currentWeapon != null)
        {
            currentWeapon.transform.position = handPlayer.position;
            currentWeapon.transform.parent = handPlayer;
            currentWeapon.transform.localEulerAngles = new Vector3(0f, 180f, 180f);
            // currentWeapon.GetComponent<>()
        
            Debug.Log("Weapon equip !");
        }
        else
        {
            Debug.LogError("Error Weapon not equip !");
        }
    }

    void DestroyWeapon()
    {
        Destroy(currentWeapon);
    }
}
