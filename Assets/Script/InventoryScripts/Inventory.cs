﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    // Can reference to only single method at a time.
    public delegate void OnItemChanged();

    public OnItemChanged onItemChangedCallback;
    // public List<Item> items = new List<Item>();
    public List<Item> items = new List<Item>();

    // Inventory can only have two items
    public const int space = 2;

    // Add an item
    public bool AddItem(Item item)
    {
        if (!item.isDefault)
        {
            // If there are already two items
            if (items.Count >= space) // items.Count < space don't work
            {
                Debug.Log("Inventory is full");
                return false;
            }
            items.Add(item);
            /*if (items[0] == null)
            {
                items[0] = item;
            }
            else
            {
                items[1] = item;
            }*/
            
            // Triggering the event OnItemChanged when the player interact with the inventory
            if (onItemChangedCallback != null)
            {
                onItemChangedCallback.Invoke();
            }
        }
        return true;
    }

    // Remove an item
    public void RemoveItem(Item item)
    {
        items.Remove(item);

        // Triggering the event OnItemChanged when the player interact with the inventory
        if (onItemChangedCallback != null)
        {
            onItemChangedCallback.Invoke();
        }
    }

    public void UseItem(Item item)
    {
        item.Use();
        RemoveItem(item);
    }
    
}
