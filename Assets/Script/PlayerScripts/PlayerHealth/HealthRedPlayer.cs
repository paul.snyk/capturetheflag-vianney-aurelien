﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthRedPlayer : MonoBehaviour
{
    public CooldownDeathScript cooldown;
    public GameObject playerCameraDeath;
    public Image imageHealth;

    public float maxHealth = 5;
    public float health = 5;
    // Start is called before the first frame update
    void Start()
    {
        health = maxHealth;
        
    }

    // Update is called once per frame
    void Update()
    {
        imageHealth.fillAmount = health / maxHealth;
        
        if (health > maxHealth)
        {
            health = maxHealth;
        }
        
        //player death
        if (health <= 0)
        {
            cooldown.CooldownRedPlayer();
            playerCameraDeath.SetActive(true);
            Destroy(this.gameObject);
        }
    }
    
    public void Damage(float currentDamage)
    {
        health -= currentDamage;
    }

    public void Healing(float currentHealing)
    {
        health += currentHealing;
    }
}
