﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CooldownDeathScript : MonoBehaviour
{
    public RespawnPlayerScript respawn;
    public float[] cooldown = new float[2];
    public bool[] respawnBool = new bool[2];

    public GameObject redPanelDeath, bluePaneldeath;

    public TMP_Text textRedCooldownDeath, textBluecooldownDeath;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //respawn red player
        if (respawnBool[0])
        {
            if (cooldown[0] > 0)
            {
                cooldown[0] -= Time.deltaTime;
                int cooldownRedInt = Mathf.RoundToInt(cooldown[0]);
                string textCooldownRed = cooldownRedInt.ToString();
                textRedCooldownDeath.text = textCooldownRed;

            }
            if (cooldown[0] <= 0)
            {
                respawn.RespawnRedPlayer();
                redPanelDeath.SetActive(false);
                respawnBool[0] = false;
            } 
        }
        
        //respawn blue player
        if (respawnBool[1])
        {
            if (cooldown[1] > 0)
            {
                cooldown[1] -= Time.deltaTime;
                int cooldownBlueInt = Mathf.RoundToInt(cooldown[1]);
                string textCooldownBlue = cooldownBlueInt.ToString();
                textRedCooldownDeath.text = textCooldownBlue;
            }
            if (cooldown[1] <= 0)
            {
                respawn.RespawnBluePlayer();
                bluePaneldeath.SetActive(false);
                respawnBool[1] = false;
            }  
        }
    }

    public void CooldownRedPlayer()
    {
        redPanelDeath.SetActive(true);
        cooldown[0] = 10f;
        respawnBool[0] = true;
    }
    public void CooldownBluePlayer()
    {
        bluePaneldeath.SetActive(true);
        cooldown[1] = 10f;
        respawnBool[1] = true;
    }
}
