﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory2 : Inventory
{
    // Singleton Inventory
    public static Inventory2 instance;

    void Awake()
    {
        if (instance != null)
        {
            // We only need one inventory instance
            Debug.LogWarning("More than one instance of Inventory1 found");
            return;
        }
        instance = this;
    }
}
