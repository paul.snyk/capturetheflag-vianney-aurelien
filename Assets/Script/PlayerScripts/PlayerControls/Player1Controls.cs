﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class Player1Controls : MonoBehaviour
{
    //input
    public Controls controls;

    public Camera camera;

    public LayerMask layerMaskItem;

    //vector
    [SerializeField] private Vector2 inputs, inputNormalized;
    private Vector3 velocity;

    //float speed/gravity
    [SerializeField] private float currentSpeed;
    private float rotation;
    public float baseSpeed = 0.025f, runSpeed = 4f, rotateSpeed = 1f;
    private float gravity = -9f, velocityY, finalVelocity = -25;

    //jump
    private bool jumping;
    private float jumpSpeed, jumpHeight = 2.5f;
    private Vector3 jumpDirection;

    //component
    private CharacterController controller;

    //controller inputs
    [SerializeField] private bool run = true, jump;

    // Interactable item
    public Interactable focusItem;
    public float countCanPickItem = 2f;
    private bool canActivatePickup = false;

    // Remove item
    public float downTime, upTime, pressTime = 0;
    public float countDown = 2.0f;
    public bool ready = false;

    // Hand
    public GameObject hand;

    // Shoes speed bonus
    public bool isSpeedBonus = false;
    private float timerBonusSpeed = 10f;

    void Start()
    {
        controller = gameObject.GetComponent<CharacterController>();
    }

    void Update()
    {
        GetInput();
        Locomotion();

        // If player 1 presses the R key
        if (Input.GetKey(controls.itemRecuperation) && countCanPickItem <= 0f)
        {
            CanTakeItem();
            countCanPickItem = 2f;
        }
        else if(countCanPickItem > 0)
        {
            countCanPickItem -= Time.deltaTime;
        }

        if (Inventory1.instance.items.Count > 0)
        {
            if (Inventory1.instance.items[0] != null)
            {
                RemoveAnItem(controls.item1);
            }

            if (Inventory1.instance.items[1] != null)
            {
                RemoveAnItem(controls.item2);
            }

            UseItem(controls.item1);

            UseItem(controls.item2);
        }

        if (isSpeedBonus)
        {
            timerBonusSpeed = timerBonusSpeed - Time.deltaTime;
            BonusSpeed();
        }
    }

    void CanTakeItem()
    {
        RaycastHit hit; 
 
        Vector3 p1 = transform.position + controller.center; 
 
        if (Physics.SphereCast(p1, controller.height / 2, transform.forward, out hit, 3)) 
        { 
            Interactable interactable = hit.collider.GetComponent<Interactable>(); 
            // If the interactable is well found 
            if (interactable != null) 
            { 
                SetFocus(interactable); 
            } 
        } 
        else 
        { 
            // If the player leaves the area of the Raycast, remove the focus item 
            RemoveFocus(); 
        } 
    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 3f);
    }
    
    void SetFocus(Interactable newFocus)
    {
        // Before, check If the new focus is not the actual focus
        if (newFocus != focusItem)
        {
            if (focusItem != null)
            {
                // if is not null, focus is defocused
                focusItem.OnDefocused();
            }

            // new focus value
            focusItem = newFocus;
        }

        newFocus.OnFocused(transform);
    }

    void RemoveFocus()
    {
        if (focusItem != null)
        {
            focusItem.OnDefocused();
        }
        focusItem = null;
    }



    void RemoveAnItem(KeyCode keyCode)
    {
        // If Key down
        if (Input.GetKeyDown(keyCode) && !ready)
        {
            downTime = Time.time;
            pressTime = downTime + countDown;
            ready = true;
        }

        // Key up
        if (Input.GetKeyUp(keyCode))
        {
            ready = false;
        }

        // If the button is pressed long enough
        if (Time.time >= pressTime && ready)
        {
            ready = false;
            if (keyCode.Equals(controls.item1))
            {
                Inventory1.instance.RemoveItem(Inventory1.instance.items[0]);
            }
            else if (keyCode.Equals(controls.item2))
            {
                Inventory1.instance.RemoveItem(Inventory1.instance.items[1]);
            }
        }
    }

    void UseItem(KeyCode keyCode)
    {
        if (Input.GetKey(keyCode).Equals(controls.item1))
        {
            Inventory1.instance.UseItem(Inventory1.instance.items[0]);
        }
        else if (Input.GetKey(keyCode).Equals(controls.item2))
        {
            Inventory1.instance.UseItem(Inventory1.instance.items[1]);
        }
    }


    void Locomotion()
    {
        //running and walking
        if (controller.isGrounded)
        {
            inputNormalized = inputs;

            currentSpeed = baseSpeed;

            if (run)
                currentSpeed *= runSpeed;
        }
        else
        {
            inputNormalized = Vector2.Lerp(inputNormalized, Vector2.zero, 0.025f);
            currentSpeed = Mathf.Lerp(currentSpeed, 0, 0.025f);
        }

        //Rotating Player
        Vector3 charactèreRotation = transform.eulerAngles + new Vector3(0, rotation * rotateSpeed, 0);
        transform.eulerAngles = charactèreRotation;


        //gravity
        if (jump && controller.isGrounded)
        {
            Jump();
        }

        if (!controller.isGrounded && velocityY > finalVelocity)
        {
            velocityY += gravity * Time.deltaTime;
        }

        //Apply Inputs
        if (!jumping)
        {
            velocity = (transform.forward * inputNormalized.y + transform.right * inputNormalized.x) * currentSpeed +
                       Vector3.up * velocityY;
        }
        else
        {
            velocity = jumpDirection * jumpSpeed + Vector3.up * velocityY;
        }


        //moving controller
        controller.Move(velocity * Time.deltaTime);

        if (controller.isGrounded)
        {
            if (jumping)
            {
                jumping = false;
            }

            velocityY = 0;
        }
    }

    void BonusSpeed()
    {
        if (timerBonusSpeed > 0)
        {
            runSpeed = 6f;
        }
        else
        {
            runSpeed = 4f;
            timerBonusSpeed = 10f;
            isSpeedBonus = false;
        }
    }

    void Jump()
    {
        //set jump true
        if (!jumping)
        {
            jumping = true;
        }

        //jump direction and speed
        jumpDirection = (transform.forward * inputs.y + transform.right * inputs.x).normalized;
        jumpSpeed = currentSpeed;

        //set velocity Y
        velocityY = Mathf.Sqrt(-gravity * jumpHeight);
    }

    void GetInput()
    {
        //Forwards and Backwards controls
        //Forwards
        if (Input.GetKey(controls.forwards))
            inputs.y = 1;

        //Backwards
        if (Input.GetKey(controls.backwards))
        {
            if (Input.GetKey(controls.forwards))
                inputs.y = 0;
            else
                inputs.y = -1;
        }

        //FW nothing
        if (!Input.GetKey(controls.forwards) && !Input.GetKey(controls.backwards))
            inputs.y = 0;

        //Strafe Left and Right
        //Strafe Left
        if (Input.GetKey(controls.strafeRight))
            inputs.x = 1;

        //Strafe Right
        if (Input.GetKey(controls.strafeLeft))
        {
            if (Input.GetKey(controls.strafeRight))
                inputs.x = 0;
            else
                inputs.x = -1;
        }

        //Strafe LR nothing
        if (!Input.GetKey(controls.strafeLeft) && !Input.GetKey(controls.strafeRight))
            inputs.x = 0;

        //Rotate Left and Right
        //Rotate Left
        if (Input.GetKey(controls.rotateRight))
        {
            rotation = 1;
        }


        //Rotate Right
        if (Input.GetKey(controls.rotateLeft))
        {
            if (Input.GetKey(controls.rotateRight))
            {
                rotation = 0;
            }

            else
            {
                rotation = -1;
            }
        }

        //Rotate LR nothing
        if (!Input.GetKey(controls.rotateLeft) && !Input.GetKey(controls.rotateRight))
        {
            rotation = 0;
        }

        //ToggleRun
        if (Input.GetKeyDown(controls.walkOrRun))
            run = !run;

        //jumping
        jump = Input.GetKey(controls.jump);
    }
}