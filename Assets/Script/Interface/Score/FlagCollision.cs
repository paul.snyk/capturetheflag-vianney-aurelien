﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagCollision : MonoBehaviour
{
    public GameObject blueSocle;

    public ScoreController score;
    public GameObject redSocle;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(blueSocle == null);
        Debug.Log(redSocle == null);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        if (redSocle == null && blueSocle != null)
        {
            if (other.gameObject.CompareTag("RedFlag"))
            {
                Debug.Log("drapeau red");
                score.BluePlayerScoreUpgrade(1);
                Destroy(other.gameObject);
            }
        }
        if (redSocle != null && blueSocle == null)
        {
            
            if (other.gameObject.CompareTag("BlueFlag"))
            {
                Debug.Log("drapeau blue");
                score.RedPlayerScoreUpgrade(1);
                Destroy(other.gameObject);
            }
        }
    }
}
