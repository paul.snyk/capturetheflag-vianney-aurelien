﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flagSpawn : MonoBehaviour
{
    public Transform redFlagPosition, blueFlagPosition;
    
    public GameObject redFlag, blueFlag;

    public GameObject flagRedSpawn, flagBlueSpawn;
    // Start is called before the first frame update
    void Start()
    {
        flagRedSpawn = Instantiate(redFlag, redFlagPosition);
        flagBlueSpawn = Instantiate(blueFlag, blueFlagPosition);
        flagBlueSpawn.transform.position = blueFlagPosition.position;
        flagRedSpawn.transform.position = redFlagPosition.position;
    }
    

    public void SpawnRedFlag()
    {
        flagRedSpawn = new GameObject();
        flagRedSpawn = Instantiate(redFlag, redFlagPosition);
        flagRedSpawn.transform.position = redFlagPosition.position;
    }
    
    public void SpawnBlueFlag()
    {
        flagBlueSpawn = new GameObject();
        flagBlueSpawn = Instantiate(blueFlag, blueFlagPosition);
        flagBlueSpawn.transform.position = blueFlagPosition.position;
    }
}
