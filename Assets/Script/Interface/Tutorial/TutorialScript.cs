﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialScript : MonoBehaviour
{
    public GameObject bluePlayer;
    public GameObject redPlayer;

    public GameObject panelGoGame;
    public GameObject[] wall;

    public bool[] phase1;
    public bool[] phase2;
    public bool[] phase3;

    public TMP_Text textPlayerBlue;
    public TMP_Text textPlayerRed;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Phase1();
        Phase2();
        Phase3();
    }

    void Phase1()
    {
        if (!phase1[1])
        {
            if (!phase1[0])
            {
                textPlayerBlue.text = "Les déplacements avec les touches ZQSD";

                if (Input.GetKey(KeyCode.Z)||Input.GetKey(KeyCode.Q)||Input.GetKey(KeyCode.S)||Input.GetKey(KeyCode.D))
                {
                    phase1[0] = true;
                }
            }
            if (phase1[0]) 
            {
                textPlayerBlue.text = "Utiliser les touches A et E pour bouger la caméra";
                if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.E))
                {
                    wall[0].SetActive(false);
                    phase1[1] = true;
                }
            } 
        }

        if (!phase1[3])
        {
            if (!phase1[2])
            {
                textPlayerRed.text = "Les déplacements avec les flèches directionnelles";

                if (Input.GetKey(KeyCode.UpArrow)||Input.GetKey(KeyCode.LeftArrow)||Input.GetKey(KeyCode.DownArrow)||Input.GetKey(KeyCode.RightArrow))
                {
                    phase1[2] = true;
                }
            }
            if (phase1[2]) 
            {
                textPlayerRed.text = "Utiliser les touches 1 et 2 du pavé numérique pour bouger la caméra";
                if (Input.GetKey(KeyCode.Keypad1) || Input.GetKey(KeyCode.Keypad2))
                {
                    wall[3].SetActive(false);
                    phase1[3] = true;
                }
            } 
        }


    }
   
    void Phase2()
    {
        if (!phase2[1]&&phase1[1])
        {
            if (!phase2[0])
            {
                textPlayerBlue.text = "";

                if (Input.GetKey(KeyCode.Z)||Input.GetKey(KeyCode.Q)||Input.GetKey(KeyCode.S)||Input.GetKey(KeyCode.D))
                {
                    phase2[0] = true;
                }
            }
            if (phase2[0]) 
            {
                textPlayerBlue.text = "";
                if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.E))
                {
                    wall[1].SetActive(false);
                    phase2[1] = true;
                }
            } 
        }

        if (!phase2[3]&&phase1[3])
        {
            if (!phase2[2])
            {
                textPlayerRed.text = "";

                if (Input.GetKey(KeyCode.UpArrow)||Input.GetKey(KeyCode.LeftArrow)||Input.GetKey(KeyCode.DownArrow)||Input.GetKey(KeyCode.RightArrow))
                {
                    phase2[2] = true;
                }
            }
            if (phase2[2]) 
            {
                textPlayerRed.text = "";
                if (Input.GetKey(KeyCode.Keypad1) || Input.GetKey(KeyCode.Keypad2))
                {
                    wall[4].SetActive(false);
                    phase2[3] = true;
                }
            } 
        }
    }
    
    void Phase3()
    {
        if (!phase3[1]&&phase2[1])
        {
            if (!phase3[0])
            {
                textPlayerBlue.text = "";

                if (Input.GetKey(KeyCode.Z)||Input.GetKey(KeyCode.Q)||Input.GetKey(KeyCode.S)||Input.GetKey(KeyCode.D))
                {
                    phase3[0] = true;
                }
            }
            if (phase3[0]) 
            {
                textPlayerBlue.text = "";
                if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.E))
                {
                    wall[2].SetActive(false);
                    phase3[1] = true;
                }
            } 
        }

        if (!phase3[3]&&phase2[3])
        {
            if (!phase3[2])
            {
                textPlayerRed.text = "";

                if (Input.GetKey(KeyCode.UpArrow)||Input.GetKey(KeyCode.LeftArrow)||Input.GetKey(KeyCode.DownArrow)||Input.GetKey(KeyCode.RightArrow))
                {
                    phase3[2] = true;
                }
            }
            if (phase3[2]) 
            {
                textPlayerRed.text = "";
                if (Input.GetKey(KeyCode.Keypad1) || Input.GetKey(KeyCode.Keypad2))
                {
                    wall[5].SetActive(false);
                    phase3[3] = true;
                }
            } 
        }  
    }

    public void SkipTuto()
    {
        SceneManager.LoadScene("Game");
    }
}
