﻿using UnityEngine;

public class ItemPickup : Interactable
{
    public Item item;

    private string playerTag;


    public override void Interact(string playerTag)
    {
        // Execut the Interact() code in the Interactable class
        base.Interact(playerTag);
        this.playerTag = playerTag;

        PickUp();
    }

    void PickUp()
    {
        // Add the item in the inventory (AddItem return true, or false if failure)
        bool wasPickedUp = false;
        switch (playerTag)
        {
            case "BluePlayer":
            {
                wasPickedUp = Inventory1.instance.AddItem(item);
                break;
            }

            case "RedPlayer":
            {
                wasPickedUp = Inventory2.instance.AddItem(item);
                break;
            }
        }

        if (wasPickedUp)
        {
            Destroy(gameObject);
        }
    }
}